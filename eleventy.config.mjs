import yaml from "js-yaml";

export default function (eleventyConfig) {
  eleventyConfig.addDataExtension("yaml", (contents) => yaml.load(contents));
  [".well-known", "assets", "style.css"].forEach((file) =>
    eleventyConfig.addPassthroughCopy(file),
  );
}
